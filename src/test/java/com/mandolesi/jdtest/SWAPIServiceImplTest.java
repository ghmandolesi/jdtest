package com.mandolesi.jdtest;

import com.mandolesi.jdtest.domain.Films;
import com.mandolesi.jdtest.domain.People;
import com.mandolesi.jdtest.domain.Species;
import com.mandolesi.jdtest.service.impl.SWAPIServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SWAPIServiceImplTest {
    @Test
    public void testGetFilm() {
        SWAPIServiceImpl client = new SWAPIServiceImpl();
        Films film = client.getFilm(1);
        assert "A New Hope".equals(film.getTitle());
    }

    @Test
    public void testGetPeople() {
        SWAPIServiceImpl client = new SWAPIServiceImpl();
        People character = client.getPeople(2);
        assert "C-3PO".equals(character.getName());
    }

    @Test
    public void testGetSpecies() {
        SWAPIServiceImpl client = new SWAPIServiceImpl();
        Species specimen = client.getSpecies(2);
        assert "Droid".equals(specimen.getName());
    }
}
