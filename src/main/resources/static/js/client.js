$(function () {
    var TOKEN_KEY = "jwtToken"
    var $notLoggedIn = $("#notLoggedIn");
    var $loggedIn = $("#loggedIn").hide();
    var $loggedInBody = $("#loggedInBody");
    var $response = $("#response");
    var $login = $("#login");
    var $userInfo = $("#userInfo").hide();
    var $searchSpecies = $("#searchSpecies").hide();
    var $results = $("#results");

    function getJwtToken() {
        return localStorage.getItem(TOKEN_KEY);
    }

    function setJwtToken(token) {
        localStorage.setItem(TOKEN_KEY, token);
    }

    function removeJwtToken() {
        localStorage.removeItem(TOKEN_KEY);
    }

    function doLogin(loginData) {
        var url_base = '/oauth/token';
        var requestPayload = {
            'grant_type': 'password',
            'username': $('#exampleInputEmail1').val(),
            'password': $('#exampleInputPassword1').val(),
            'client_id': 'testjwtclientid',
            'client_secret': 'XY7kmzoNzl100',
            'scope': ''
        }
        $.ajax({
            'url': url_base,
            'type': 'POST',
            'content-Type': 'x-www-form-urlencoded',
            'dataType': 'json',
            'headers': {
                'Authorization': 'basic dGVzdGp3dGNsaWVudGlkOlhZN2ttem9OemwxMDA='
            },
            'data': requestPayload,
            'success': function (result) {
                setJwtToken(result.access_token);
                $login.hide();
                $notLoggedIn.hide();
                showTokenInformation();
                $userInfo.show();
                $searchSpecies.show();
                $results.empty();
            },
            'error': function (XMLHttpRequest, textStatus, errorThrown) {
                $('#loginErrorModal')
                    .modal("show")
                    .find(".modal-body")
                    .empty()
                    .html("<p>Spring exception:<br>" + jqXHR.responseJSON.exception + "</p>");
            }
        });
    }

    function doLogout() {
        removeJwtToken();
        $results.empty();
        $login.show();
        $userInfo
            .hide()
            .find("#userInfoBody").empty();
        $loggedIn.hide();
        $searchSpecies.hide();
        $loggedInBody.empty();
        $notLoggedIn.show();
    }

    function createAuthorizationTokenHeader() {
        var token = getJwtToken();
        if (token) {
            return {"Authorization": "Bearer " + token};
        } else {
            return {};
        }
    }

    function showTokenInformation() {
        var jwtToken = getJwtToken();
        var decodedToken = jwt_decode(jwtToken);
        $loggedInBody.append($("<h4>").text("Token"));
        $loggedInBody.append($("<div>").text(jwtToken).css("word-break", "break-all"));
        $loggedIn.show();
    }

    function appendKeyValue($table, key, value) {
        var $row = $("<tr>")
            .append($("<td>").text(key))
            .append($("<td>").text(value));
        $table.append($row);
    }

    function showResponse(statusCode, message) {
        $response
            .empty()
            .text("status code: " + statusCode + "\n-------------------------\n" + message);
    }

    function createResultList(data, charName) {
        var html = '<div class="panel-body"><label>Characters of the same species found:</label><ol class="list-group">';
        for (var i = 0; i < data.length; i++) {
            var name = data[i].name;
            if (name != charName) {
                var li = '<li class="list-group-item">' + name + '</li>';
                html = html + li;
            }
        }
        html = html + "</ol></div>";
        return html;
    }

    $("#loginForm").submit(function (event) {
        event.preventDefault();
        var $form = $(this);
        var formData = {
            username: $form.find('input[name="username"]').val(),
            password: $form.find('input[name="password"]').val()
        };
        doLogin(formData);
    });

    $("#logoutButton").click(doLogout);

    $("#searchBtn").click(function () {
        $results.empty();
        var filmSelected = $('#sel1 option:selected').val();
        var charSelected = $('#sel2 option:selected').val();
        var charName = $('#sel2 option:selected').html();
        console.log(charName);
        var url = "/api/jdtest/characters?film_id=" + filmSelected + "&character_id=" + charSelected;

        $.ajax({
            url: url,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader(),
            success: function (data, textStatus, jqXHR) {
                showResponse(jqXHR.status, JSON.stringify(data));
                if (data.length > 1) {
                    var html = createResultList(data, charName);
                    $("#results").append(html);
                } else {
                    var html = '<div class="panel-body"><label>No characters of the same species found.</label></div>';
                    $("#results").append(html);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                showResponse(jqXHR.status, errorThrown);
            }
        });
    });

    $("#exampleServiceBtn").click(function () {
        $.ajax({
            url: "/persons",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            headers: createAuthorizationTokenHeader(),
            success: function (data, textStatus, jqXHR) {
                showResponse(jqXHR.status, JSON.stringify(data));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                showResponse(jqXHR.status, errorThrown);
            }
        });
    });

    $("#adminServiceBtn").click(function () {
        $.ajax({
            url: "/protected",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            headers: createAuthorizationTokenHeader(),
            success: function (data, textStatus, jqXHR) {
                showResponse(jqXHR.status, data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                showResponse(jqXHR.status, errorThrown);
            }
        });
    });

    $loggedIn.click(function () {
        $loggedIn
            .toggleClass("text-hidden")
            .toggleClass("text-shown");
    });

    if (getJwtToken()) {
        $login.hide();
        $notLoggedIn.hide();
        showTokenInformation();
        $userInfo.show();
        $searchSpecies.show();
    }
});