INSERT INTO app_role (id, role_name, description) VALUES (1, 'STANDARD_USER', 'Standard User');

-- USER
-- non-encrypted password: jwtpass
INSERT INTO app_user (id, first_name, last_name, password, username) VALUES (1, 'John', 'Deere', '$2a$10$qtH0F1m488673KwgAfFXEOWxsoZSeHqqlB/8BTt3a6gsI5c2mdlfe', 'john.deere');
INSERT INTO app_user (id, first_name, last_name, password, username) VALUES (2, 'Gustavo', 'Mandolesi', '$2a$10$qtH0F1m488673KwgAfFXEOWxsoZSeHqqlB/8BTt3a6gsI5c2mdlfe', 'gustavo.mandolesi');

INSERT INTO user_role(user_id, role_id) VALUES(1,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,1);