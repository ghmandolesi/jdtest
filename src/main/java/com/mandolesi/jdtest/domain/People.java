package com.mandolesi.jdtest.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Simple class to represent Star Wars characters, containing data related to a unique being.
 * <p>
 * Ref.: https://swapi.co/documentation#people
 */
@Data
@NoArgsConstructor
public class People implements Serializable {

    private String name;

    private String birthYear;

    private String eyeColor;

    private String gender;

    private String hairColor;

    private String height;

    private String mass;

    private String skinColor;

    private String homeWorld;

    private List<String> films;

    private List<String> species;

    private List<String> starships;

    private List<String> vehicles;

    private String url;

    private String created;

    private String edited;
}
