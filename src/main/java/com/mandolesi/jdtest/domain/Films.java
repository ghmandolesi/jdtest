package com.mandolesi.jdtest.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Simple class to represent Star Wars episodes information, containing data related to a specific film.
 * <p>
 * Ref.: https://swapi.co/documentation#films
 */
@Data
//@Builder
@NoArgsConstructor
public class Films implements Serializable {

    private String title;

    private int episodeId;

    private String openingCrawl;

    private String director;

    private String producer;

    private String releaseDate;

    private List<String> species;

    private List<String> starships;

    private List<String> vehicles;

    private List<String> planets;

    private List<String> characters;

    private String url;

    private String created;

    private String edited;
}
