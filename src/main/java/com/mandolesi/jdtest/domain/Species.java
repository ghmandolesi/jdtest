package com.mandolesi.jdtest.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Simple class to represent a type of person or character within Star Wars.
 * <p>
 * Ref.: https://swapi.co/documentation#species
 */
@Data
@NoArgsConstructor
public class Species implements Serializable {

    private String name;

    private String classification;

    private String designation;

    private String averageHeight;

    private String averageLifespan;

    private String eyeColors;

    private String hairColors;

    private String skinColors;

    private String language;

    private String homeWorld;

    private List<String> people;

    private List<String> films;

    private String url;

    private String created;

    private String edited;
}
