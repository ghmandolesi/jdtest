package com.mandolesi.jdtest.controller.advice;

import com.mandolesi.jdtest.domain.ApiError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    //    @ExceptionHandler({ UserNotFoundException.class, ContentNotAllowedException.class })
    @ExceptionHandler()
    public final ResponseEntity<ApiError> handleException(Exception ex, WebRequest request) {
        logger.error("Handling " + ex.getClass().getSimpleName() + " due to " + ex.getMessage());

        HttpHeaders headers = new HttpHeaders();
        List<String> errors = Collections.singletonList(ex.getMessage());

        if (ex instanceof HttpClientErrorException.NotFound) {
            HttpStatus status = HttpStatus.NOT_FOUND;
            ResourceNotFoundException nex = new ResourceNotFoundException();
            return handleExceptionInternal(nex, new ApiError(errors), headers, status, request);
        }

        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        return handleExceptionInternal(ex, new ApiError(errors), headers, status, request);
    }

    protected ResponseEntity<ApiError> handleExceptionInternal(Exception ex, ApiError body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        }

        return new ResponseEntity<>(body, headers, status);
    }
}
