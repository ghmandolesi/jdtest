package com.mandolesi.jdtest.controller.advice;

public class ResourceNotFoundException extends Exception {

    @Override
    public String getMessage() {
        return "Could not find information based on your parameters.";
    }
}
