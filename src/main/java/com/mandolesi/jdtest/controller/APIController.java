package com.mandolesi.jdtest.controller;

import com.mandolesi.jdtest.domain.People;
import com.mandolesi.jdtest.service.SpeciesInFilmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * ---> /api/jdtest
 */
@RestController
@RequestMapping(APIController.API)
public class APIController {

    private static final Logger logger = LoggerFactory.getLogger(APIController.class);

    static final String API = "/api/jdtest";

    static final String CHARACTERS = "/characters";

    @Autowired
    SpeciesInFilmService speciesInFilmService;

    @RequestMapping(value = CHARACTERS, method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('STANDARD_USER')")
    public ResponseEntity<List<People>> getCharacter(@RequestParam(name = "film_id") int filmId, @RequestParam(name = "character_id") int characterId) {

        logger.debug("Request received (film_id=" + filmId + ", character_id=" + characterId + ")" );

        List<People> sameSpecies = speciesInFilmService.find(filmId, characterId);

        return new ResponseEntity<List<People>>(sameSpecies, HttpStatus.OK);
    }
}
