package com.mandolesi.jdtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.mandolesi.jdtest"})
@EnableCaching
@EnableJpaRepositories("com.mandolesi.jdtest.repository")
public class JDTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JDTestApplication.class, args);
	}

}
