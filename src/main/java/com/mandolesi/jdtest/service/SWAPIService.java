package com.mandolesi.jdtest.service;

import com.mandolesi.jdtest.domain.Films;
import com.mandolesi.jdtest.domain.People;
import com.mandolesi.jdtest.domain.Species;

/**
 * Contract to be implemented by service classes that consume SWAPI data.
 */
public interface SWAPIService {

    public Films getFilm(int id);

    public People getPeople(int id);

    public Species getSpecies(int id);
}
