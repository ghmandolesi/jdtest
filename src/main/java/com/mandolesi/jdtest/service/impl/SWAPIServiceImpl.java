package com.mandolesi.jdtest.service.impl;

import com.mandolesi.jdtest.domain.Films;
import com.mandolesi.jdtest.domain.People;
import com.mandolesi.jdtest.domain.Species;
import com.mandolesi.jdtest.service.SWAPIService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class SWAPIServiceImpl implements SWAPIService {

    private static final Logger logger = LoggerFactory.getLogger(SWAPIServiceImpl.class);

    private static final String ENDPOINT = "https://swapi.co/api/";

    public static final String FILMS = "films";

    public static final String PEOPLE = "people";

    public static final String SPECIES = "species";

    @Override
    @Cacheable("films")
    public Films getFilm(int id) {
        logger.debug("Getting film ID:" + id + " information from SWAPI");
        ResponseEntity<Films> response = new RestTemplate().exchange(getFilmsUrl(id), HttpMethod.GET, getEntity(), new ParameterizedTypeReference<Films>() {
        });
        return response.getBody();
    }

    @Override
    @Cacheable("people")
    public People getPeople(int id) {
        logger.debug("Getting character ID:" + id + " information from SWAPI");
        ResponseEntity<People> response = new RestTemplate().exchange(getPeopleUrl(id), HttpMethod.GET, getEntity(), new ParameterizedTypeReference<People>() {
        });
        return response.getBody();
    }

    @Override
    @Cacheable("species")
    public Species getSpecies(int id) {
        logger.debug("Getting species ID:" + id + " information from SWAPI");
        ResponseEntity<Species> response = new RestTemplate().exchange(getSpeciesUrl(id), HttpMethod.GET, getEntity(), new ParameterizedTypeReference<Species>() {
        });
        return response.getBody();
    }

    private HttpEntity<String> getEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        return entity;
    }

    private String getFilmsUrl(int id) {
        return ENDPOINT + FILMS + "/" + id;
    }

    private String getPeopleUrl(int id) {
        return ENDPOINT + PEOPLE + "/" + id;
    }

    private String getSpeciesUrl(int id) {
        return ENDPOINT + SPECIES + "/" + id;
    }
}
