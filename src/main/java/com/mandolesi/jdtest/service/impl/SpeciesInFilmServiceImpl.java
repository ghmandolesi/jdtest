package com.mandolesi.jdtest.service.impl;

import com.mandolesi.jdtest.domain.Films;
import com.mandolesi.jdtest.domain.People;
import com.mandolesi.jdtest.service.SWAPIService;
import com.mandolesi.jdtest.service.SpeciesInFilmService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SpeciesInFilmServiceImpl implements SpeciesInFilmService {

    private static final Logger logger = LoggerFactory.getLogger(SpeciesInFilmServiceImpl.class);

    @Autowired
    SWAPIService swapiService;

    @Override
    public List<People> find(int filmId, int characterId) {

        logger.debug("Trying to find information for film ID:" + filmId + " and character ID:" + characterId);

        People character = swapiService.getPeople(characterId);

        logger.debug("Character ID:" + characterId + " is " + character.getName());

        validateSpeciesForPeople(character);

        String characterSpeciesUrl = character.getSpecies().get(0);
        logger.debug(character.getName() + " species URL is " + characterSpeciesUrl);

        Films film = swapiService.getFilm(filmId);
        logger.debug("Film ID:" + filmId + " title is '" + film.getTitle() + "'");

        List<String> characters = film.getCharacters();

        validateFilmCharacters(characters);
        logger.debug("Film '" + film.getTitle() + "' has " + characters.size() + " characters");

        List<People> sameSpecies = new ArrayList<>();

        for (String peopleUrl : characters) {

            int id = getIdFromPeopleUrl(peopleUrl);

            People otherCharacter = swapiService.getPeople(id);
            logger.debug("Character ID:" + id + " is " + otherCharacter.getName());

            validateSpeciesForPeople(otherCharacter);

            String otherSpeciesUrl = otherCharacter.getSpecies().get(0);
            logger.debug(otherCharacter.getName() + " species URL is " + otherSpeciesUrl);

            if (characterSpeciesUrl.equals(otherSpeciesUrl)) {
                logger.debug(otherCharacter.getName() + " is of the same species as " + character.getName() + "!");
                sameSpecies.add(otherCharacter);
            }
        }

        return sameSpecies;
    }

    private void validateFilmCharacters(List<String> characters) {
        if (characters.isEmpty()) {
            // TODO: throw exception "Film has no characters"
        }
    }

    private void validateSpeciesForPeople(People character) {
        if (character.getSpecies().isEmpty()) {
            // TODO: throw exception here "Empty species returned for ID:" + characterId
        }
    }

    private int getIdFromPeopleUrl(String url) {
        String[] parts = url.split("/");
        if (parts.length != 6) {
            // TODO: Throw exception "Invalid People URL"
        }
        return Integer.parseInt(parts[5]);
    }
}
