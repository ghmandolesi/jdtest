package com.mandolesi.jdtest.service.impl;

import com.mandolesi.jdtest.domain.User;
import com.mandolesi.jdtest.repository.UserRepository;
import com.mandolesi.jdtest.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenericServiceImpl implements GenericService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> findAllUsers() {
        return (List<User>) userRepository.findAll();
    }
}
