package com.mandolesi.jdtest.service;

import com.mandolesi.jdtest.domain.User;

import java.util.List;

public interface GenericService {

    User findByUsername(String username);

    List<User> findAllUsers();
}
