package com.mandolesi.jdtest.service;

import com.mandolesi.jdtest.domain.People;

import java.util.List;

public interface SpeciesInFilmService {

    public List<People> find(int filmId, int characterId);
}
