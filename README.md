# JDTest

## Description

A simple system to expose Star Wars character data.

## Build

```
mvn clean package -Dmaven.test.skip=true 
```

## Run
```
java -jar target/jdtest-0.0.1-SNAPSHOT.jar
```

## How to test the application

### Web page

Access ```http://localhost:8080```

Credentials for login are:

- Username: **john.deere**
- Password: **jwtpass**

### Direct API requests

The following basic pieces of information are needed:

 * Client: testjwtclientid
 * Secret: XY7kmzoNzl100
 * Username and password defined above
 

1. Generate an access token

Use the following generic command to generate an access token:
   
`$ curl client:secret@localhost:8080/oauth/token -d grant_type=password -d username=user -d password=pwd`

For this specific application, to generate an access token for the non-admin user john.deere, run:
   
`$ curl testjwtclientid:XY7kmzoNzl100@localhost:8080/oauth/token -d grant_type=password -d username=john.deere -d password=jwtpass`

A response similar to the following will be generated:

`
{"access_token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsidGVzdGp3dHJlc291cmNlaWQiXSwidXNlcl9uYW1lIjoiam9obi5kZWVyZSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJleHAiOjE1NzA0NzA1OTUsImF1dGhvcml0aWVzIjpbIlNUQU5EQVJEX1VTRVIiXSwianRpIjoiNTdmYmJkMzEtYWNiMi00Mzk3LThhMDAtMTIyNGI5NzU4MjIxIiwiY2xpZW50X2lkIjoidGVzdGp3dGNsaWVudGlkIn0.zLkzlupt6wc64U7Esljw0pPH-huK_gYA2RRxq8glSWw","token_type":"bearer","expires_in":43199,"scope":"read write","jti":"57fbbd31-acb2-4397-8a00-1224b9758221"}
`

2. Use the token to access resources through the RESTful API

Use the generated token  as the value of the Bearer in the Authorization header as follows:

`curl  http://localhost:8080/api/jdtest/characters?film_id=1&character_id=2 -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsidGVzdGp3dHJlc291cmNlaWQiXSwidXNlcl9uYW1lIjoiYWRtaW4uYWRtaW4iLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiZXhwIjoxNDk0NDU0MjgyLCJhdXRob3JpdGllcyI6WyJTVEFOREFSRF9VU0VSIiwiQURNSU5fVVNFUiJdLCJqdGkiOiIwYmQ4ZTQ1MC03ZjVjLTQ5ZjMtOTFmMC01Nzc1YjdiY2MwMGYiLCJjbGllbnRfaWQiOiJ0ZXN0and0Y2xpZW50aWQifQ.rvEAa4dIz8hT8uxzfjkEJKG982Ree5PdUW17KtFyeec" 


## Architecture

### Layers

The application code was structured in packages that represent the following logical layers:

* config
* controller
* domain
* repository
* service
* persistence

It's a fairly common and even "classic" structure, so not too much details here.

## Considerations & Comments

It was very interesting to have participated in this challenge by developing the application. The short term closely resembles a real project scenario as well as the level of detail of the requirements presented, which has led to the need for rapid decision making during development, considering project information at hand.

I did my best in the time available, always trying to reconcile work on the project with family living on the weekend. ;o)

I appreciate the opportunity and hope the project can meet expectations.